import { useState } from "react";
import Router from "next/router";
import { GetServerSideProps } from "next";
import { Magic } from "magic-sdk";
import Button from "../src/components/reusables/button";
import { getLoginSession } from "../src/lib/auth";
import Form from "../src/components/reusables/form";
import Input from "../src/components/reusables/form/Input";

const Login = () => {
  const [errorMsg, setErrorMsg] = useState("");
  const [loading, setLoading] = useState(false);
  async function handleSubmit(values: { email: string }) {
    if (errorMsg) setErrorMsg("");
    setLoading(true);
    const body = {
      email: values.email,
    };

    try {
      const magic = new Magic(process.env.NEXT_PUBLIC_MAGIC_PUBLISHABLE_KEY);
      const didToken = await magic.auth.loginWithMagicLink({
        email: body.email,
      });
      const res = await fetch("/api/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + didToken,
        },
        body: JSON.stringify(body),
      });
      setLoading(false);
      if (res.status === 200) {
        Router.push("/");
      } else {
        throw new Error(await res.text());
      }
    } catch (error) {
      setLoading(false);
      console.error("An unexpected error happened occurred:", error);
      setErrorMsg(error.message);
    }
  }

  return (
    <section className="h-full grid content-center justify-items-center">
      <div className="bg-gray-500 rounded-lg w-1/3 p-5 py-10 ">
        <div className="flex justify-center">
          <span className="text-gray-400 font-extrabold">
            <i className="text-yellow-500 text-base">COL</i>IN
          </span>
        </div>

        <Form onFinish={handleSubmit} layout="vertical">
          <Form.Item name="email" label="Email Address">
            <Input
              size="large"
              placeholder="email address"
              className="bg-black text-white focus:outline-none px-3"
            />
          </Form.Item>
          <div className="mt-5">
            <Button loading={loading} className="w-full">
              submit
            </Button>
          </div>
        </Form>
        {errorMsg && (
          <span className="text-red-500 block my-2">{errorMsg}</span>
        )}
      </div>
    </section>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const user = await getLoginSession(context.req);

  if (user) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};
Login.auth = true;
export default Login;
