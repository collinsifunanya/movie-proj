import React from "react";
import { GetServerSideProps } from "next";
import VideoCapsule from "../../src/components/reusables/card/VideoCapsule";
import Search from "../../src/components/reusables/search";
import Tabs from "../../src/components/reusables/tabs";
import Movies from "../../src/lib/dataStore";
import { getLoginSession } from "../../src/lib/auth";
const { TabPane } = Tabs;
function AllShows() {
  return (
    <main>
      <section className="container m-auto py-20 relative">
        <div className="absolute top-10 right-0 hidden lg:block w-1/3">
          <Search />
        </div>
        <Tabs>
          <TabPane tab="All" key="all">
            <div className="grid grid-cols-4 gap-5">
              {Movies.map(
                ({
                  id,
                  title,
                  poster_path,
                  release_date,
                  vote_count,
                  genre_ids,
                }) => (
                  <VideoCapsule
                    id={id}
                    key={id}
                    title={title}
                    poster_path={poster_path}
                    release_date={release_date}
                    vote_count={vote_count}
                    genre={genre_ids[0]}
                  />
                )
              )}
            </div>
          </TabPane>

          <TabPane tab="Sport" key="sport">
            <div className="grid grid-cols-4 gap-5">
              {Array.from(Movies)
                .slice(10, 20)
                .map(
                  ({
                    id,
                    title,
                    poster_path,
                    release_date,
                    vote_count,
                    genre_ids,
                  }) => (
                    <VideoCapsule
                      id={id}
                      key={id}
                      title={title}
                      poster_path={poster_path}
                      release_date={release_date}
                      vote_count={vote_count}
                      genre={genre_ids[0]}
                    />
                  )
                )}
            </div>
          </TabPane>

          <TabPane tab="Action" key="action">
            <div className="grid grid-cols-4 gap-5">
              {Array.from(Movies)
                .slice(20, 30)
                .map(
                  ({
                    id,
                    title,
                    poster_path,
                    release_date,
                    vote_count,
                    genre_ids,
                  }) => (
                    <VideoCapsule
                      key={id}
                      id={id}
                      title={title}
                      poster_path={poster_path}
                      release_date={release_date}
                      vote_count={vote_count}
                      genre={genre_ids[0]}
                    />
                  )
                )}
            </div>
          </TabPane>

          <TabPane tab="Drama" key="drama">
            <div className="grid grid-cols-4 gap-5">
              {Array.from(Movies)
                .slice(10, 20)
                .map(
                  ({
                    id,
                    title,
                    poster_path,
                    release_date,
                    vote_count,
                    genre_ids,
                  }) => (
                    <VideoCapsule
                      key={id}
                      id={id}
                      title={title}
                      poster_path={poster_path}
                      release_date={release_date}
                      vote_count={vote_count}
                      genre={genre_ids[0]}
                    />
                  )
                )}
            </div>
          </TabPane>
        </Tabs>
      </section>
    </main>
  );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const user = await getLoginSession(context.req);

  if (!user) {
    return {
      redirect: {
        destination: "/login",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};

export default AllShows;
