import { useRouter } from "next/router";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import VideoCapsule from "../../../src/components/reusables/card/VideoCapsule";
import CommentBox from "../../../src/components/reusables/commentBox";
import Label from "../../../src/components/reusables/label";
import VideoPlayer from "../../../src/components/reusables/videoPlayer";
import { GetServerSideProps } from "next";
import { getLoginSession } from "../../../src/lib/auth";
import Movies from "../../../src/lib/dataStore";
type movieState = {
  overview: string;
  backdrop_path: string;
  release_date: string;
};
function Movie() {
  const [movie, setMovie] = useState<movieState>({} as movieState);
  const { query } = useRouter();
  const { slug } = query;

  useEffect(() => {
    const data = Movies.find(({ id }) => id === Number(slug));
    if (data) {
      const { overview, backdrop_path, release_date } = data;
      setMovie({ overview, backdrop_path, release_date });
    }
  }, [slug]);

  const { overview, backdrop_path, release_date } = movie;
  return (
    <main className=" container m-auto py-20">
      <section className="grid grid-cols-3 gap-5">
        <div className="lg:col-span-2 col-span-3 order-1">
          <VideoPlayer
            poster={backdrop_path}
            url="/video/rad.mp4"
            subtitileUrl="/video/subtitle/rad.mp4"
          />
        </div>
        <div className="lg:col-span-1 col-span-3  p-3 bg-gray-800 rounded-lg lg:order-2 order-5">
          <CommentBox />
        </div>
        <div className="lg:col-span-2 col-span-3  p-3 self-start order-3">
          <div className="flex space-x-3">
            <span className="text-green-400 font-extrabold">90% Match</span>
            <span>{release_date}</span>
            <Label text="family" primary />
            <Label text="HD" secondary />
            <Label text="+18" secondary />
          </div>

          <p className="my-5">{overview}</p>
        </div>
        <div className="lg:col-span-1 col-span-3 order-4">
          <span className="space-x-2 block">
            <span className="text-gray-300">Genre :</span>
            <span className="text-gray-400">Football, Sport</span>
          </span>

          <span className="space-x-2">
            <span className="text-gray-300">Audio :</span>
            <span className="text-gray-400">English</span>
          </span>

          <div className="my-5 space-y-2">
            <p className=" font-extrabold text-white text-lg">
              Director & Cast
            </p>
            <div className="flex space-x-2">
              <Label text="SuperSport" secondary className="rounded-full" />
              <Label text="EPL" secondary className="rounded-full" />
            </div>
          </div>
        </div>
      </section>

      <section className="my-20">
        <span className="font-bold text-base my-3 block">Related Shows</span>

        <div className="grid grid-cols-4 gap-5">
          {Array.from(Movies)
            .slice(9, 13)
            .map(
              ({
                id,
                title,
                poster_path,
                release_date,
                vote_count,
                genre_ids,
              }) => (
                <VideoCapsule
                  key={id}
                  id={id}
                  title={title}
                  poster_path={poster_path}
                  release_date={release_date}
                  vote_count={vote_count}
                  genre={genre_ids[0]}
                />
              )
            )}
        </div>
      </section>
    </main>
  );
}

export default Movie;
