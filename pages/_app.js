import "../styles/globals.css";
import Nav from "../src/components/LandingPageComponents/Nav";
function MyApp({ Component, pageProps }) {
  return (
    <>
      {!Component.auth && <Nav />}
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
