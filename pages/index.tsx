import HeaderSection from "../src/components/LandingPageComponents/HeaderSection";
import Catlogues from "../src/components/LandingPageComponents/Catlogues";

export default function Home() {
  return (
    <>
      <HeaderSection />
      <main className="container m-auto py-10">
        <Catlogues />
      </main>
    </>
  );
}
