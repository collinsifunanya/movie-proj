/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./pages/**/*.{js,ts,jsx,tsx}", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    container: {
      center: true,
    },

    container: {
      padding: {
        DEFAULT: "1rem",
        lg: "2rem",
        "2xl": "3rem",
      },
    },
    extend: {},
  },
  plugins: [],
};
