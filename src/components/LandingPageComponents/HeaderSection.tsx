import React from "react";
import icons from "../../icons/index";
import Label from "../reusables/label/index";

function HeaderSection() {
  return (
    <header className="header-hero h-4/5  ">
      <section className="grid grid-cols-2 content-end pb-20 h-full container m-auto">
        <div className=" col-span-2 md:col-span-1 space-y-3">
          <div className="flex  gap-3">
            <Label secondary text="TV Shows" />
            <Label secondary text="Sci-Fi TV" />
            <Label secondary text="TV Horror" />
          </div>
          <p className="text-sm text-gray-300 w-full lg:w-3/5">
            Nancy has sobering visions, and El passes an important test. Back in
            Hawkins, the gang gathers supplies and prepares for battle.
          </p>
        </div>
        <div className="col-span-2 md:col-span-1 flex items-center space-x-2">
          <span className=" font-extrabold text-lg">Watch now</span>
          <span>{icons["circelplay"]}</span>
        </div>
      </section>
    </header>
  );
}

export default HeaderSection;
