import Link from "next/link";
import React from "react";

function Nav() {
  return (
    <nav className="flex justify-between  container m-auto py-3">
      <Link href="/">
        <span className="text-gray-400 font-extrabold cursor-pointer">
          <i className="text-yellow-500 text-base">COL</i>IN
        </span>
      </Link>
      <div className="flex space-x-3 items-center">
        <span className="text-gray-400">collinsogbuzuru@gmail.com</span>
        <span className="h-8 w-8 flex justify-center items-center rounded-full bg-gray-600">
          @
        </span>
        <span className="text-gray-400 cursor-pointer">Logout</span>
      </div>
    </nav>
  );
}

export default Nav;
