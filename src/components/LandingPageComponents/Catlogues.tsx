import VideoCapsule from "../reusables/card/VideoCapsule";
import Rate from "../reusables/Rating/index";
import Movies from "../../lib/dataStore";
import Link from "next/link";
function TopRated() {
  return (
    <>
      <section>
        <div className="flex justify-between">
          <div className="flex items-center space-x-2">
            <span className="font-bold text-base ">Top rated</span>
            <span className="h-5 w-5">
              <Rate rate={1} count={1} />
            </span>
          </div>

          <Link href="/tv">
            <span className=" cursor-pointer hover:text-yellow-400">
              See more
            </span>
          </Link>
        </div>

        <div className="grid grid-cols-4 gap-5 my-4 overflow-x-scroll">
          {Array.from(Movies)
            .splice(0, 4)
            .map(
              ({
                id,
                title,
                poster_path,
                release_date,
                vote_count,
                genre_ids,
              }) => (
                <VideoCapsule
                  key={id}
                  id={id}
                  title={title}
                  poster_path={poster_path}
                  release_date={release_date}
                  vote_count={vote_count}
                  genre={genre_ids[0]}
                />
              )
            )}
        </div>
      </section>

      <section className="my-10">
        <div className="flex justify-between">
          <div className="flex items-center space-x-2">
            <span className="font-bold text-base ">Recommended</span>
            <span className="h-5 w-5">
              <Rate rate={1} count={0} />
            </span>
          </div>
          <Link href="/tv">
            <span className=" cursor-pointer hover:text-yellow-400">
              See more
            </span>
          </Link>
        </div>

        <div className="grid grid-cols-4 gap-5 my-4">
          {Array.from(Movies)
            .slice(4, 8)
            .map(
              ({
                id,
                title,
                poster_path,
                release_date,
                vote_count,
                genre_ids,
              }) => (
                <VideoCapsule
                  key={id}
                  id={id}
                  title={title}
                  poster_path={poster_path}
                  release_date={release_date}
                  vote_count={vote_count}
                  genre={genre_ids[0]}
                />
              )
            )}
        </div>
      </section>

      <section className="my-10">
        <div className="flex justify-between">
          <div className="flex items-center space-x-2">
            <span className="font-bold text-base ">Recommended</span>
            <span className="h-5 w-5">
              <Rate rate={1} count={0} />
            </span>
          </div>

          <Link href="/tv">
            <span className=" cursor-pointer hover:text-yellow-400">
              See more
            </span>
          </Link>
        </div>
        <div className="grid grid-cols-4 gap-5 my-4">
          {Array.from(Movies)
            .slice(9, 13)
            .map(
              ({
                id,
                title,
                poster_path,
                release_date,
                vote_count,
                genre_ids,
              }) => (
                <VideoCapsule
                  key={id}
                  id={id}
                  title={title}
                  poster_path={poster_path}
                  release_date={release_date}
                  vote_count={vote_count}
                  genre={genre_ids[0]}
                />
              )
            )}
        </div>
      </section>
    </>
  );
}

export default TopRated;
