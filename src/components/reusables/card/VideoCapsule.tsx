import { useRouter } from "next/router";
import React from "react";
import Button from "../button/index";
import Rate from "../Rating/index";

type VideoCapsuleProps = {
  poster_path: string;
  title: string;
  release_date: string;
  vote_count: number;
  genre: number;
  id: number;
};
function VideoCapsule({
  id,
  poster_path,
  title,
  release_date,
  vote_count,
  genre,
}: VideoCapsuleProps) {
  const { push } = useRouter();
  return (
    <div className="aspect-square relative rounded-2xl overflow-hidden text-white grid grid-cols-1 p-5">
      <img
        src={`https://image.tmdb.org/t/p/original${poster_path}`}
        className="  absolute object-cover"
        alt="absolute"
      />
      <div className="relative">
        <span className="bg-black w-fit rounded-full  p-1 px-2 space-x-2 flex items-center">
          <span className="h-4 w-4 block">
            <Rate count={1} rate={1} />
          </span>
          <span>{vote_count}</span>
        </span>
      </div>
      <div className="relative self-end space-y-6">
        <div>
          <span
            onClick={() => push(`/tv/${genre}/${id}`)}
            className="font-extrabold md:text-3xl text-xl cursor-pointer"
          >
            {title}
          </span>
          <span className="text-gray-300 font-light block text-lg">
            {release_date}
          </span>
        </div>

        <div className="flex justify-between items-center">
          <Button
            onClick={() => push(`/tv/${genre}/${id}`)}
            primary
            className="rounded-full"
          >
            Watch now
          </Button>
          <div className="bg-gray-400 rounded-full h-8 w-8 grid text-xl justify-items-center content-center flex-shrink-0">
            +
          </div>
        </div>
      </div>
    </div>
  );
}

export default VideoCapsule;
