import React, { createContext, Children, useEffect, useState } from "react";
const tabsContext = createContext(null);
type tabsprops = {
  defaultActiveKey?: string;
  onChange?: () => void;
  className?: string;
  style?: React.CSSProperties;
  children: React.ReactNode;
};

type tabsPaneprops = {
  tab: string;
  key: string;
  children: React.ReactNode;
};
function Tabs({
  defaultActiveKey,
  onChange,
  className,
  style,
  children,
}: tabsprops) {
  const [currentActiveKey, setCurrentActiveKey] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    const firstTab = children[0].key;
    if (!isLoaded && defaultActiveKey) {
      setCurrentActiveKey(defaultActiveKey);
      setIsLoaded(false);
      return;
    }

    setCurrentActiveKey(firstTab);
    setIsLoaded(false);
  }, [defaultActiveKey]);

  useEffect(() => {
    if (isLoaded) {
      onChange();
    }
  }, [currentActiveKey]);

  return (
    <div style={{ ...style }} className={`${className} space-y-10 `}>
      <div className={`flex space-x-5 md:space-x-10  overflow-x-scroll `}>
        {Children.map(children, (child: React.ReactElement) => (
          <span
            className={`cursor-pointer font-poppins font-semibold space text-base tabname block ${
              child.key === currentActiveKey ? "active" : ""
            } relative h-7`}
            onClick={() => setCurrentActiveKey(child.key)}
            key={child.key}
          >
            {child.props.tab}
          </span>
        ))}
      </div>
      {/* body of tabs */}
      <div>
        {Children.map(children, (child: React.ReactElement) => (
          <div
            className={`${child.key === currentActiveKey ? "block" : "hidden"}`}
            key={child.key}
          >
            {child.props.children}
          </div>
        ))}
      </div>
    </div>
  );
}

function TabPane({ tab, key, children }: tabsPaneprops) {
  return (
    <div key={key} className="flex flex-col space-y-2">
      <span>{tab}</span>
      <div>{children}</div>
    </div>
  );
}

Tabs.TabPane = TabPane;
export default Tabs;
