import React from "react";
import Button from "../button";

function Search(props) {
  return (
    <div className="flex  group-hover:bg-red-400   p-1 bg-inherit border border-ev_slate lg:w-10/12 md:w-2/4 justify-between overflow-hidden rounded-full md:mt-8 mt-5 relative">
      <input
        required
        name="search"
        type="search"
        placeholder="search..."
        className="text-ev_gray group  bg-transparent outline-none border-none shadow-none focus:ring-transparent w-full active:bg-transparent px-2"
      />
      <Button
        style={{ borderRadius: "100%" }}
        aria-label="submit button"
        primary
        icon="search"
        className="hover:-translate-y-0 flex-shrink-0  h-10 w-10 text-white"
      />
    </div>
  );
}

export default Search;
