import React from "react";
import Icons from "../../../icons/index";

type buttonProps = {
  icon?: string;
  className?: string;
  style?: React.CSSProperties;
  text?: string;
  children?: React.ReactNode;
  loading?: boolean;
  iconPosition?: "left" | "right";
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
};

type PrimaryButtonProps = {
  primary?: boolean;
  secondary?: never;
  default?: never;
  link?: never;
};

type SecondaryButtonProps = {
  primary?: never;
  secondary?: boolean;
  default?: never;
  link?: never;
};

type DefaultButtonProps = {
  primary?: never;
  secondary?: never;
  default?: boolean;
  link?: never;
};

type LinkButtonProps = {
  primary?: never;
  secondary?: never;
  default?: never;
  link?: boolean;
};
type combineProps = buttonProps &
  (
    | SecondaryButtonProps
    | PrimaryButtonProps
    | DefaultButtonProps
    | LinkButtonProps
  );

const createClassNames = (classes: { [key: string]: boolean }): string => {
  let classNames = "";
  for (const [key, value] of Object.entries(classes)) {
    if (value) classNames += key + " ";
  }
  return classNames.trim();
};

function Button({
  icon,
  text,
  children,
  iconPosition = "left",
  primary: btnprimary = true,
  secondary: btnsecondary = false,
  default: btndefault = false,
  link: btnlink = false,
  className,
  style,
  loading = false,
  onClick = () => null,
  ...rest
}: combineProps) {
  const styleIconPosition = `${
    iconPosition === "left" ? "order-1" : "order-2"
  }`;

  const styleTextPosition = `${
    iconPosition === "left" ? "order-2" : "order-1"
  }`;

  const types = createClassNames({
    btnprimary,
    btnlink,
    btndefault,
    btnsecondary,
  });

  return (
    <button
      {...rest}
      style={style}
      onClick={onClick}
      className={`btn  ${className} ${types} `}
    >
      {!loading && (
        <span className={styleIconPosition}>{icon && Icons[icon]}</span>
      )}
      {loading && <span className="self-center block">{Icons["loader"]}</span>}
      {text ||
        (children && (
          <span className={styleTextPosition}>{text ? text : children}</span>
        ))}
    </button>
  );
}

export default Button;
