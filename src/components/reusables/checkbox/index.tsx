import React from "react";

function CheckBox(props: React.HTMLProps<HTMLInputElement>) {
  return (
    <label className="switch">
      <input {...props} type="checkbox" />
      <span className="slider round"></span>
    </label>
  );
}

export default CheckBox;
