import dynamic from "next/dynamic";
import icons from "../../../icons";

const ReactPlayer = dynamic(() => import("react-player/file"), {
  ssr: false,
});

type VideoProps = {
  poster: string;
  url: string;
  subtitileUrl: string;
};

function Video({ poster, url, subtitileUrl }: VideoProps) {
  return (
    <div className="w-full rounded-lg overflow-hidden border-2  border-yellow-400">
      <ReactPlayer
        previewTabIndex={3}
        light={`https://image.tmdb.org/t/p/original${poster}`}
        playing={true}
        playIcon={
          <span className="text-yellow-500">{icons["circelplay"]}</span>
        }
        controls
        height={"50vh"}
        width="100%"
        // url="http://d3rlna7iyyu8wu.cloudfront.net/skip_armstrong/skip_armstrong_stereo_subs.m3u8"
        url={url}
        config={{
          tracks: [
            {
              label: "Rad video",
              kind: "subtitles",
              src: subtitileUrl,
              srcLang: "en",
            },
          ],
        }}
      />
    </div>
  );
}

export default Video;
