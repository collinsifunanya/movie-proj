import React from "react";
import { useState } from "react";
import Button from "../button";
import CheckBox from "../checkbox";
import Form from "../form";

type commentType = {
  author: string;
  avi: string;
  text: string;
};
function CommentBox() {
  const [isCommentEnabled, setIsCommentEnabled] = useState(false);
  const [comments, setComment] = useState<commentType[]>([]);

  const commentHandler = (value: { text: string }) => {
    const { text } = value;
    setComment((prev) => [...prev, { text, author: "Joe Doe", avi: "rrrrrr" }]);
  };
  return (
    <div className="h-full grid">
      <div className="flex justify-end space-x-2 mb-1 items-center self-start ">
        <span className="text-gray-400 text-xs">
          {isCommentEnabled ? "Disable comment" : "Enable comment"}
        </span>
        <CheckBox
          onClick={(e) =>
            setIsCommentEnabled((e.target as HTMLInputElement).checked)
          }
        />
      </div>

      {isCommentEnabled && (
        <div className="bg-gray-700 h-80 overflow-hidden overflow-y-scroll">
          {comments.map(({ text, author, avi }) => (
            <div key={text} className="p-2 flex space-x-2">
              <div className="h-8 w-8 bg-gray-500 rounded-full flex justify-center items-center">
                <span>s</span>
              </div>
              <span className="text-gray-500">{author}</span>
              <p>{text}</p>
            </div>
          ))}
        </div>
      )}

      {!isCommentEnabled && (
        <div className="bg-gray-700 h-80 overflow-hidden overflow-y-scroll flex flex-col justify-center">
          <p className="text-center">comments is dsiabled</p>
        </div>
      )}

      {isCommentEnabled && (
        <Form onFinish={commentHandler}>
          <div className="flex p-1 bg-white w-full justify-between rounded-sm  relative self-end">
            <input
              type="text"
              name="text"
              placeholder="Say something..."
              className="text-black outline-none border-none shadow-none focus:ring-transparent w-full mx-2"
            />
            <Button
              style={{ borderRadius: 0 }}
              primary
              icon="send"
              className="hover:-translate-y-0 flex-shrink-0"
            />
          </div>
        </Form>
      )}
    </div>
  );
}

export default CommentBox;
