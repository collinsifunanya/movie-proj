import React from "react";
import icons from "../../../icons/index";

function Rate({ rate, count }: { rate: number; count: number }) {
  const starCount = new Array(count);

  const handleCreateRateStar = () => {
    const ratings = [];
    for (let i = 0; i < count; i++) {
      ratings.push({ nodeKey: i, active: i + 1 <= rate });
    }
    return ratings;
  };

  return (
    <div>
      {handleCreateRateStar().map(({ nodeKey, active }) => (
        <span
          key={nodeKey}
          className={`${active ? "text-yellow-400" : "text-gray-400"}`}
        >
          {icons["star"]}
        </span>
      ))}
    </div>
  );
}

export default Rate;
