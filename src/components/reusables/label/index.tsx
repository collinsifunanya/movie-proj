import React from "react";

type LabelProp = {
  text: string;
  className?: string;
};

type PrimaryLabelProps = {
  primary: boolean;
  secondary?: never;
  destructive?: never;
};

type SecondaryLabelProps = {
  primary?: never;
  secondary: boolean;
  destructive?: never;
};

type DestructiveLabelProps = {
  primary?: never;
  secondary?: never;
  destructive: boolean;
};

const createClassNames = (classes: { [key: string]: boolean }): string => {
  let classNames = "";
  for (const [key, value] of Object.entries(classes)) {
    if (value) classNames += key + " ";
  }
  return classNames.trim();
};

function Label({
  text,
  className,
  primary: primarylabel = false,
  secondary: secondarylabel = false,
  destructive: destructivelabel = false,
}: LabelProp &
  (PrimaryLabelProps | SecondaryLabelProps | DestructiveLabelProps)) {
  const classNames = createClassNames({
    primarylabel,
    secondarylabel,
    destructivelabel,
  });
  return (
    <div className={` label text-sm ${classNames} ${className}`}>{text}</div>
  );
}

export default Label;
