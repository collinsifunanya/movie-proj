import { NextSeo } from "next-seo";

function SEO({ url, title, description }) {
  // const siteURL = "http://rapidloans.ng";

  const siteURL = process.env.NEXT_PUBLIC_WEBSITE_URL;
  return (
    <NextSeo
      title={title || ""}
      description={description || ""}
      canonical=""
      openGraph={{
        url: siteURL || "",
        title: title || "",
        description: description || "",
        images: [
          {
            url: ``,
            width: 800,
            height: 600,
            alt: "",
            type: "image/jpeg",
          },
          {
            url: ``,
            width: 900,
            height: 800,
            alt: "",
            type: "image/jpeg",
          },
          { url: `` },
        ],
        site_name: "",
      }}
      twitter={{
        handle: "",
        site: "",
        cardType: "",
      }}
    />
  );
}

export default SEO;
