const Movies = [
  {
    adult: false,
    backdrop_path: "/1UK9JS0uDpGm6y9XOBbHjSrrdrp.jpg",
    genre_ids: [878, 35],
    id: 982987,
    media_type: "movie",
    title: "Blasted",
    original_language: "no",
    original_title: "Blasted - Gutta vs. Aliens",
    overview:
      "When a former childhood friend crashes Sebastian's bachelor party and makes it all about himself, only an alien invasion can make them put aside their bad blood and reunite as the kick-ass laser-tag duo they once were.",
    popularity: 21.055,
    poster_path: "/aEB9ogCBCUpSFTZVhD7NvTh44VW.jpg",
    release_date: "2022-06-22",
    video: false,
    vote_average: 5.7,
    vote_count: 11,
  },
  {
    adult: false,
    backdrop_path: "/hZTU9xlOl1pyZxCDM6mDCLZqZcy.jpg",
    genre_ids: [10749, 18, 10402],
    id: 667642,
    media_type: "movie",
    title: "Beauty",
    original_language: "en",
    original_title: "Beauty",
    overview:
      "A young singer on the brink of a promising career finds herself torn between a domineering family, industry pressures and her love for her girlfriend.",
    popularity: 11.138,
    poster_path: "/yjAXHyMz0JZzYiQP7q1fT5kXLif.jpg",
    release_date: "2022-06-11",
    video: false,
    vote_average: 7.222,
    vote_count: 9,
  },
  {
    adult: false,
    backdrop_path: "/bszpg7dIDSap4hrJHMBk1NgpqHg.jpg",
    genre_ids: [53, 28],
    id: 916719,
    media_type: "movie",
    title: "Code Name Banshee",
    original_language: "en",
    original_title: "Code Name Banshee",
    overview:
      "Caleb, a former government assassin in hiding, who resurfaces when his protégé, the equally deadly killer known as Banshee, discovers a bounty has been placed on Caleb's head.",
    popularity: 31.515,
    poster_path: "/lxkvlIrvExeHGiPk7bdji2xm3vs.jpg",
    release_date: "2022-07-01",
    video: false,
    vote_average: 7.5,
    vote_count: 2,
  },
  {
    adult: false,
    backdrop_path: "/wcKFYIiVDvRURrzglV9kGu7fpfY.jpg",
    genre_ids: [14, 28, 12],
    id: 453395,
    media_type: "movie",
    title: "Doctor Strange in the Multiverse of Madness",
    original_language: "en",
    original_title: "Doctor Strange in the Multiverse of Madness",
    overview:
      "Doctor Strange, with the help of mystical allies both old and new, traverses the mind-bending and dangerous alternate realities of the Multiverse to confront a mysterious new adversary.",
    popularity: 10084.004,
    poster_path: "/61PVJ06oecwvcBisoAQu6SDfdcS.jpg",
    release_date: "2022-05-04",
    video: false,
    vote_average: 7.554,
    vote_count: 3769,
  },
  {
    adult: false,
    backdrop_path: "/2gGVdr0BcKzma68SW2jpZfJtOlB.jpg",
    genre_ids: [10751, 16, 28, 12, 35],
    id: 438148,
    media_type: "movie",
    title: "Minions: The Rise of Gru",
    original_language: "en",
    original_title: "Minions: The Rise of Gru",
    overview:
      "A fanboy of a supervillain supergroup known as the Vicious 6, Gru hatches a plan to become evil enough to join them, with the backup of his followers, the Minions.",
    popularity: 1408.742,
    poster_path: "/wKiOkZTN9lUUUNZLmtnwubZYONg.jpg",
    release_date: "2022-06-16",
    video: false,
    vote_average: 7.875,
    vote_count: 64,
  },
  {
    adult: false,
    backdrop_path: "/pFP878NvW4MD6MQbRv0tqpdoBXY.jpg",
    genre_ids: [878, 53],
    id: 793961,
    media_type: "movie",
    title: "Rubikon",
    original_language: "en",
    original_title: "Rubikon",
    overview:
      "The company soldier Hannah and the scientists Gavin and Dimitri are researching an algae project on board the Rubikon space station, which is supposed to permanently supply humanity with oxygen and food. But suddenly the earth disappears below them in a brown, toxic fog and all contact is broken off - are they the last survivors of humanity? Should they initiate the safe station, dare the return flight and thereby risk their lives?",
    popularity: 27.612,
    poster_path: "/fn819ItxSuSxHIpm052pRFlPwLj.jpg",
    release_date: "2022-07-01",
    video: false,
    vote_average: 6.3,
    vote_count: 3,
  },
  {
    adult: false,
    backdrop_path: "/5PnypKiSj2efSPqThNjTXz8jwOg.jpg",
    genre_ids: [14, 28],
    id: 759175,
    media_type: "movie",
    title: "The Princess",
    original_language: "en",
    original_title: "The Princess",
    overview:
      "A beautiful, strong-willed young royal refuses to wed the cruel sociopath to whom she is betrothed and is kidnapped and locked in a remote tower of her father’s castle. With her scorned, vindictive suitor intent on taking her father’s throne, the princess must protect her family and save the kingdom.",
    popularity: 19.14,
    poster_path: "/gt9s8TtZZ36TXF1CE1y19rSpOZu.jpg",
    release_date: "2022-06-16",
    video: false,
    vote_average: 6.778,
    vote_count: 9,
  },
  {
    adult: false,
    backdrop_path: "/iOFBH9KtjKMntbP2kheeOpMQTcC.jpg",
    genre_ids: [28, 35, 53],
    id: 667739,
    media_type: "movie",
    title: "The Man From Toronto",
    original_language: "en",
    original_title: "The Man From Toronto",
    overview:
      "In a case of mistaken identity, the world’s deadliest assassin, known as the Man from Toronto, and a New York City screw-up are forced to team up after being confused for each other at an Airbnb.",
    popularity: 577.181,
    poster_path: "/5TdKvZimLSJHPQW8t3ctlsusnmH.jpg",
    release_date: "2022-06-24",
    video: false,
    vote_average: 6.123,
    vote_count: 166,
  },
  {
    adult: false,
    backdrop_path: "/p1F51Lvj3sMopG948F5HsBbl43C.jpg",
    genre_ids: [28, 12, 14],
    id: 616037,
    media_type: "movie",
    title: "Thor: Love and Thunder",
    original_language: "en",
    original_title: "Thor: Love and Thunder",
    overview:
      "After his retirement is interrupted by Gorr the God Butcher, a galactic killer who seeks the extinction of the gods, Thor enlists the help of King Valkyrie, Korg, and ex-girlfriend Jane Foster, who now inexplicably wields Mjolnir as the Mighty Thor. Together they embark upon a harrowing cosmic adventure to uncover the mystery of the God Butcher’s vengeance and stop him before it’s too late.",
    popularity: 820.608,
    poster_path: "/4zLfBbGnuUBLbMVtagTZvzFwS8l.jpg",
    release_date: "2022-07-06",
    video: false,
    vote_average: 6.9,
    vote_count: 9,
  },
  {
    adult: false,
    backdrop_path: "/odJ4hx6g6vBt4lBWKFD1tI8WS4x.jpg",
    genre_ids: [28, 18],
    id: 361743,
    media_type: "movie",
    title: "Top Gun: Maverick",
    original_language: "en",
    original_title: "Top Gun: Maverick",
    overview:
      "After more than thirty years of service as one of the Navy’s top aviators, and dodging the advancement in rank that would ground him, Pete “Maverick” Mitchell finds himself training a detachment of TOP GUN graduates for a specialized mission the likes of which no living pilot has ever seen.",
    popularity: 1000.323,
    poster_path: "/62HCnUTziyWcpDaBO2i1DX17ljH.jpg",
    release_date: "2022-05-24",
    video: false,
    vote_average: 8.378,
    vote_count: 1360,
  },
  {
    adult: false,
    backdrop_path: "/8Z8dVGTn3OhX1AR49tgvh89i79G.jpg",
    genre_ids: [37, 28, 53],
    id: 884315,
    media_type: "movie",
    title: "Terror on the Prairie",
    original_language: "en",
    original_title: "Terror on the Prairie",
    overview:
      "A pioneering family fights back against a gang of vicious outlaws that is terrorizing them on their newly-built farm on the plains of Montana.",
    popularity: 18.994,
    poster_path: "/rPWaktPt50BwkYHmYM62sDrdnjR.jpg",
    release_date: "2022-06-14",
    video: false,
    vote_average: 7.3,
    vote_count: 10,
  },
  {
    adult: false,
    backdrop_path: "/56v2KjBlU4XaOv9rVYEQypROD7P.jpg",
    genre_ids: [18, 10765, 9648],
    id: 66732,
    media_type: "tv",
    name: "Stranger Things",
    origin_country: ["US"],
    original_language: "en",
    original_name: "Stranger Things",
    overview:
      "When a young boy vanishes, a small town uncovers a mystery involving secret experiments, terrifying supernatural forces, and one strange little girl.",
    popularity: 3421.944,
    poster_path: "/49WJfeN0moxb9IPfGn8AIqMGskD.jpg",
    first_air_date: "2016-07-15",
    vote_average: 8.616,
    vote_count: 11389,
  },
  {
    adult: false,
    backdrop_path: "/eqLwFXX0R7U0crdgQcj0Rxjb9uc.jpg",
    genre_ids: [10759, 18],
    id: 120911,
    media_type: "tv",
    name: "The Terminal List",
    origin_country: ["US"],
    original_language: "en",
    original_name: "The Terminal List",
    overview:
      "Navy SEAL Commander James Reece turns to vengeance as he investigates the mysterious forces behind the murder of his entire platoon. Free from the military’s command structure, Reece applies the lessons he’s learned from nearly two decades of warfare to hunt down the people responsible.",
    popularity: 158.781,
    poster_path: "/ujVz6YZxBkDsw3wanO2AKhl3A9y.jpg",
    first_air_date: "2022-06-30",
    vote_average: 8.636,
    vote_count: 11,
  },
  {
    adult: false,
    backdrop_path: "/4BlS2ojR7ueEYsBUNi6reY4livW.jpg",
    genre_ids: [16, 10759, 35, 10762],
    id: 114466,
    media_type: "tv",
    name: "Baymax!",
    origin_country: ["US"],
    original_language: "en",
    original_name: "Baymax!",
    overview:
      "Return to the fantastical city of San Fransokyo where the affable, inflatable, inimitable healthcare companion robot, Baymax, sets out to do what he was programmed to do: help others.",
    popularity: 86.003,
    poster_path: "/muzgYvvFniQnwiTdAvfMCEkHsT4.jpg",
    first_air_date: "2022-06-29",
    vote_average: 7.609,
    vote_count: 23,
  },
  {
    adult: false,
    backdrop_path: "/n6vVs6z8obNbExdD3QHTr4Utu1Z.jpg",
    genre_ids: [10765, 10759],
    id: 76479,
    media_type: "tv",
    name: "The Boys",
    origin_country: ["US"],
    original_language: "en",
    original_name: "The Boys",
    overview:
      "A group of vigilantes known informally as “The Boys” set out to take down corrupt superheroes with no more than blue-collar grit and a willingness to fight dirty.",
    popularity: 3786.662,
    poster_path: "/stTEycfG9928HYGEISBFaG1ngjM.jpg",
    first_air_date: "2019-07-25",
    vote_average: 8.447,
    vote_count: 6349,
  },
  {
    adult: false,
    backdrop_path: "/rlCRM7U5g2hcU1O8ylGcqsMYHIP.jpg",
    genre_ids: [10765, 10759, 35],
    id: 92782,
    media_type: "tv",
    name: "Ms. Marvel",
    origin_country: ["US"],
    original_language: "en",
    original_name: "Ms. Marvel",
    overview:
      "A great student, avid gamer, and voracious fan-fic scribe, Kamala Khan has a special affinity for superheroes, particularly Captain Marvel. However, she struggles to fit in at home and at school — that is, until she gets superpowers like the heroes she’s always looked up to. Life is easier with superpowers, right?",
    popularity: 2820.589,
    poster_path: "/cdkyMYdu8ao26XOBvilNzLneUg1.jpg",
    first_air_date: "2022-06-08",
    vote_average: 7.7,
    vote_count: 213,
  },
  {
    adult: false,
    backdrop_path: "/vHTuFN4uN1jD1xQWHnOft5dLvL5.jpg",
    genre_ids: [10765, 37],
    id: 63247,
    media_type: "tv",
    name: "Westworld",
    origin_country: ["US"],
    original_language: "en",
    original_name: "Westworld",
    overview:
      "A dark odyssey about the dawn of artificial consciousness and the evolution of sin. Set at the intersection of the near future and the reimagined past, it explores a world in which every human appetite, no matter how noble or depraved, can be indulged.",
    popularity: 255.397,
    poster_path: "/8MfgyFHf7XEboZJPZXCIDqqiz6e.jpg",
    first_air_date: "2016-10-02",
    vote_average: 8.12,
    vote_count: 4166,
  },
  {
    adult: false,
    backdrop_path: "/oJeQTOiAmUcO9MWkURLVaByDbXL.jpg",
    genre_ids: [16, 10759, 10765],
    id: 156898,
    media_type: "tv",
    name: "Bastard!! -Heavy Metal, Dark Fantasy-",
    origin_country: ["JP"],
    original_language: "ja",
    original_name: "BASTARD!! －暗黒の破壊神－",
    overview:
      "When evil forces threaten to resurrect Anthrasax, the God of Destruction, the Kingdom of Meta-llicana calls on a volatile dark wizard for help.",
    popularity: 54.323,
    poster_path: "/dvzUTBvOzJsnQOyozWIfglSm9zN.jpg",
    first_air_date: "2022-06-30",
    vote_average: 5.3,
    vote_count: 3,
  },
  {
    adult: false,
    backdrop_path: "/oFm97FvIcVJqnhHgciUKhuP4xzn.jpg",
    genre_ids: [35],
    id: 197588,
    media_type: "tv",
    name: "Man Vs Bee",
    origin_country: ["GB"],
    original_language: "en",
    original_name: "Man Vs Bee",
    overview:
      "A man finds himself at war with a bee while house-sitting a luxurious mansion. Who will win, and what irreparable damage will be done in the process?",
    popularity: 516.956,
    poster_path: "/c80mxgOPvSWJBcgWoI5bf3et5Ws.jpg",
    first_air_date: "2022-06-24",
    vote_average: 7.363,
    vote_count: 95,
  },

  {
    adult: false,
    backdrop_path: "/lcTuggU70y6pt6x13Rv1Ffjs93K.jpg",
    genre_ids: [10759, 80, 18, 9648],
    id: 112836,
    media_type: "tv",
    name: "Money Heist: Korea - Joint Economic Area",
    origin_country: ["KR"],
    original_language: "ko",
    original_name: "종이의 집: 공동경제구역",
    overview:
      "Disguised under the shadows of a mask, a crew of desperados band together under the leadership of a criminal mastermind known only as “The Professor” to pull off the biggest heist Korea has ever seen.",
    popularity: 1749.206,
    poster_path: "/mbsRGqJtdKcVbjQxkrfzKCAkYoU.jpg",
    first_air_date: "2022-06-24",
    vote_average: 8.208,
    vote_count: 72,
  },
  {
    adult: false,
    backdrop_path: "/zGLHX92Gk96O1DJvLil7ObJTbaL.jpg",
    genre_ids: [14, 12, 28],
    id: 338953,
    media_type: "movie",
    title: "Fantastic Beasts: The Secrets of Dumbledore",
    original_language: "en",
    original_title: "Fantastic Beasts: The Secrets of Dumbledore",
    overview:
      "Professor Albus Dumbledore knows the powerful, dark wizard Gellert Grindelwald is moving to seize control of the wizarding world. Unable to stop him alone, he entrusts magizoologist Newt Scamander to lead an intrepid team of wizards and witches. They soon encounter an array of old and new beasts as they clash with Grindelwald's growing legion of followers.",
    popularity: 2818.865,
    poster_path: "/jrgifaYeUtTnaH7NF5Drkgjg2MB.jpg",
    release_date: "2022-04-06",
    video: false,
    vote_average: 6.84,
    vote_count: 2086,
  },
  {
    adult: false,
    backdrop_path: "/etoVmQauLgUnu34qlqUPlOkevZO.jpg",
    genre_ids: [10765, 10759, 18],
    id: 92830,
    media_type: "tv",
    name: "Obi-Wan Kenobi",
    origin_country: ["US"],
    original_language: "en",
    original_name: "Obi-Wan Kenobi",
    overview:
      "During the reign of the Galactic Empire, former Jedi Master, Obi-Wan Kenobi, embarks on a crucial mission to confront allies turned enemies and face the wrath of the Empire.",
    popularity: 1465.379,
    poster_path: "/qJRB789ceLryrLvOKrZqLKr2CGf.jpg",
    first_air_date: "2022-05-26",
    vote_average: 7.733,
    vote_count: 497,
  },
  {
    adult: false,
    backdrop_path: "/wvdiTDDBxI0lix4Wbc2qePfgkA2.jpg",
    genre_ids: [35, 80, 9648],
    id: 107113,
    media_type: "tv",
    name: "Only Murders in the Building",
    origin_country: ["US"],
    original_language: "en",
    original_name: "Only Murders in the Building",
    overview:
      "Three strangers who share an obsession with true crime suddenly find themselves wrapped up in one.",
    popularity: 262.978,
    poster_path: "/ArsVbYhS6DiFJtRAlV8ytAauNlp.jpg",
    first_air_date: "2021-08-31",
    vote_average: 8.6,
    vote_count: 643,
  },
  {
    adult: false,
    backdrop_path: "/iEi7zXoVUVXYNeVxBzdt68jegqD.jpg",
    genre_ids: [53, 27],
    id: 990982,
    media_type: "movie",
    title: "Last the Night",
    original_language: "en",
    original_title: "Last the Night",
    overview:
      "During the pandemic, a burned-out teacher on the verge of a breakdown overhears his students making fun of him in a Zoom class and vows revenge.",
    popularity: 1.4,
    poster_path: "/jZS8IDXT7zCoNvRE6LD9yBMPE34.jpg",
    release_date: "2022-07-01",
    video: false,
    vote_average: 0,
    vote_count: 0,
  },
  {
    adult: false,
    backdrop_path: "/wo3l9p0S7pcvwlzlndtKgq0peRJ.jpg",
    genre_ids: [28, 12, 878],
    id: 507086,
    media_type: "movie",
    title: "Jurassic World Dominion",
    original_language: "en",
    original_title: "Jurassic World Dominion",
    overview:
      "Four years after Isla Nublar was destroyed, dinosaurs now live—and hunt—alongside humans all over the world. This fragile balance will reshape the future and determine, once and for all, whether human beings are to remain the apex predators on a planet they now share with history’s most fearsome creatures.",
    popularity: 1920.879,
    poster_path: "/kAVRgw7GgK1CfYEJq8ME6EvRIgU.jpg",
    release_date: "2022-06-01",
    video: false,
    vote_average: 6.653,
    vote_count: 885,
  },
  {
    adult: false,
    backdrop_path: "/4N2FuCTuqH3h4dw9XPFtsnDPSz7.jpg",
    genre_ids: [27, 18],
    id: 780609,
    media_type: "movie",
    title: "Men",
    original_language: "en",
    original_title: "Men",
    overview:
      "In the aftermath of a personal tragedy, Harper retreats alone to the beautiful English countryside, hoping to find a place to heal. But someone — or something — from the surrounding woods appears to be stalking her, and what begins as simmering dread becomes a fully-formed nightmare, inhabited by her darkest memories and fears.",
    popularity: 94.504,
    poster_path: "/jo1Kv3P3UgDVk7JnUFr2Cl8WWUM.jpg",
    release_date: "2022-05-20",
    video: false,
    vote_average: 6.244,
    vote_count: 119,
  },
  {
    adult: false,
    backdrop_path: "/iQFcwSGbZXMkeyKrxbPnwnRo5fl.jpg",
    genre_ids: [28, 12, 878],
    id: 634649,
    media_type: "movie",
    title: "Spider-Man: No Way Home",
    original_language: "en",
    original_title: "Spider-Man: No Way Home",
    overview:
      "Peter Parker is unmasked and no longer able to separate his normal life from the high-stakes of being a super-hero. When he asks for help from Doctor Strange the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man.",
    popularity: 1927.454,
    poster_path: "/1g0dhYtq4irTY1GPXvft6k4YLjm.jpg",
    release_date: "2021-12-15",
    video: false,
    vote_average: 8.066,
    vote_count: 13857,
  },
  {
    adult: false,
    backdrop_path: "/xHrp2pq73oi9D64xigPjWW1wcz1.jpg",
    genre_ids: [80, 9648, 53],
    id: 414906,
    media_type: "movie",
    title: "The Batman",
    original_language: "en",
    original_title: "The Batman",
    overview:
      "In his second year of fighting crime, Batman uncovers corruption in Gotham City that connects to his own family while facing a serial killer known as the Riddler.",
    popularity: 1169.242,
    poster_path: "/74xTEgt7R36Fpooo50r9T25onhq.jpg",
    release_date: "2022-03-01",
    video: false,
    vote_average: 7.76,
    vote_count: 5373,
  },
  {
    adult: false,
    backdrop_path: "/7CnFLRHr6Ml5Vtfg2B7CxgUFOcY.jpg",
    genre_ids: [18, 10402, 36],
    id: 614934,
    media_type: "movie",
    title: "Elvis",
    original_language: "en",
    original_title: "Elvis",
    overview:
      "The life story of Elvis Presley as seen through the complicated relationship with his enigmatic manager, Colonel Tom Parker.",
    popularity: 229.115,
    poster_path: "/qBOKWqAFbveZ4ryjJJwbie6tXkQ.jpg",
    release_date: "2022-06-22",
    video: false,
    vote_average: 7.832,
    vote_count: 214,
  },
  {
    adult: false,
    backdrop_path: "/fZ5lYDijd5t6YPrawg0b5JBVbyQ.jpg",
    genre_ids: [16, 878, 12, 28, 10751],
    id: 718789,
    media_type: "movie",
    title: "Lightyear",
    original_language: "en",
    original_title: "Lightyear",
    overview:
      "Legendary Space Ranger Buzz Lightyear embarks on an intergalactic adventure alongside a group of ambitious recruits and his robot companion Sox.",
    popularity: 1120.897,
    poster_path: "/vpILbP9eOQEtdQgl4vgjZUNY07r.jpg",
    release_date: "2022-06-15",
    video: false,
    vote_average: 7.087,
    vote_count: 352,
  },
  {
    adult: false,
    backdrop_path: "/ciToI1GfkWmzavJIPXh9Xe0Neon.jpg",
    genre_ids: [10759, 10765, 18],
    id: 75006,
    media_type: "tv",
    name: "The Umbrella Academy",
    origin_country: ["US"],
    original_language: "en",
    original_name: "The Umbrella Academy",
    overview:
      "A dysfunctional family of superheroes comes together to solve the mystery of their father's death, the threat of the apocalypse and more.",
    popularity: 1710.904,
    poster_path: "/scZlQQYnDVlnpxFTxaIv2g0BWnL.jpg",
    first_air_date: "2019-02-15",
    vote_average: 8.696,
    vote_count: 8005,
  },
  {
    adult: false,
    backdrop_path: "/GLLgrghu7wSPaSjl2Rw4kjASOJ.jpg",
    genre_ids: [27, 53],
    id: 756999,
    media_type: "movie",
    title: "The Black Phone",
    original_language: "en",
    original_title: "The Black Phone",
    overview:
      "Finney Shaw, a shy but clever 13-year-old boy, is abducted by a sadistic killer and trapped in a soundproof basement where screaming is of little use. When a disconnected phone on the wall begins to ring, Finney discovers that he can hear the voices of the killer’s previous victims. And they are dead set on making sure that what happened to them doesn’t happen to Finney.",
    popularity: 1773.238,
    poster_path: "/bxHZpV02OOu9vq3sb3MsOudEnYc.jpg",
    release_date: "2022-06-22",
    video: false,
    vote_average: 7.182,
    vote_count: 151,
  },
  {
    adult: false,
    backdrop_path: "/7JcaVugJ6wxr4DPvLRxwjOekHch.jpg",
    genre_ids: [28, 12, 878],
    id: 545611,
    media_type: "movie",
    title: "Everything Everywhere All at Once",
    original_language: "en",
    original_title: "Everything Everywhere All at Once",
    overview:
      "An aging Chinese immigrant is swept up in an insane adventure, where she alone can save what's important to her by connecting with the lives she could have led in other universes.",
    popularity: 360.065,
    poster_path: "/w3LxiVYdWWRvEVdn5RYq6jIqkb1.jpg",
    release_date: "2022-03-24",
    video: false,
    vote_average: 8.327,
    vote_count: 806,
  },
  {
    adult: false,
    backdrop_path: "/gG9fTyDL03fiKnOpf2tr01sncnt.jpg",
    genre_ids: [28, 878, 14],
    id: 526896,
    media_type: "movie",
    title: "Morbius",
    original_language: "en",
    original_title: "Morbius",
    overview:
      "Dangerously ill with a rare blood disorder, and determined to save others suffering his same fate, Dr. Michael Morbius attempts a desperate gamble. What at first appears to be a radical success soon reveals itself to be a remedy potentially worse than the disease.",
    popularity: 2038.496,
    poster_path: "/6JjfSchsU6daXk2AKX8EEBjO3Fm.jpg",
    release_date: "2022-03-30",
    video: false,
    vote_average: 6.454,
    vote_count: 1890,
  },
  {
    adult: false,
    backdrop_path: "/uKL39lSf3rQbcMG8XbByoHBwGId.jpg",
    genre_ids: [18],
    id: 759054,
    media_type: "movie",
    title: "Rise",
    original_language: "en",
    original_title: "Rise",
    overview:
      "After emigrating to Greece from Nigeria, Vera and Charles Antetokounmpo struggled to survive and provide for their five children, while living under the daily threat of deportation. Desperate to obtain Greek citizenship but undermined by a system that blocked them at every turn, the family's vision, determination and faith lifted them out of obscurity to launch the careers of three NBA champions.",
    popularity: 439.436,
    poster_path: "/czXyMcmiulyZjhYAnnMwVhFCvN.jpg",
    release_date: "2022-06-23",
    video: false,
    vote_average: 7.3,
    vote_count: 48,
  },
  {
    adult: false,
    backdrop_path: "/egoyMDLqCxzjnSrWOz50uLlJWmD.jpg",
    genre_ids: [28, 12, 10751, 35],
    id: 675353,
    media_type: "movie",
    title: "Sonic the Hedgehog 2",
    original_language: "en",
    original_title: "Sonic the Hedgehog 2",
    overview:
      "After settling in Green Hills, Sonic is eager to prove he has what it takes to be a true hero. His test comes when Dr. Robotnik returns, this time with a new partner, Knuckles, in search for an emerald that has the power to destroy civilizations. Sonic teams up with his own sidekick, Tails, and together they embark on a globe-trotting journey to find the emerald before it falls into the wrong hands.",
    popularity: 2387.591,
    poster_path: "/6DrHO1jr3qVrViUO6s6kFiAGM7.jpg",
    release_date: "2022-03-30",
    video: false,
    vote_average: 7.711,
    vote_count: 2317,
  },
  {
    adult: false,
    backdrop_path: "/5tmjv1moohN65IuHAbYjKw3cw7I.jpg",
    genre_ids: [878, 53],
    id: 615469,
    media_type: "movie",
    title: "Spiderhead",
    original_language: "en",
    original_title: "Spiderhead",
    overview:
      "A prisoner in a state-of-the-art penitentiary begins to question the purpose of the emotion-controlling drugs he's testing for a pharmaceutical genius.",
    popularity: 1682.375,
    poster_path: "/7COPO5B9AOKIB4sEkvNu0wfve3c.jpg",
    release_date: "2022-06-15",
    video: false,
    vote_average: 5.685,
    vote_count: 461,
  },
  {
    adult: false,
    backdrop_path: "/lfQnJr9ugRKtl3vhG6IfOOAhN8s.jpg",
    genre_ids: [18],
    id: 197067,
    media_type: "tv",
    name: "Extraordinary Attorney Woo",
    origin_country: ["KR"],
    original_language: "ko",
    original_name: "이상한 변호사 우영우",
    overview:
      "Brilliant attorney Woo Young-woo tackles challenges in the courtroom and beyond as a newbie at a top law firm and a woman on the autism spectrum.",
    popularity: 66.475,
    poster_path: "/1bInSWX10mdONmTjSRvsLjfVR2O.jpg",
    first_air_date: "2022-06-29",
    vote_average: 7.333,
    vote_count: 3,
  },
  {
    adult: false,
    backdrop_path: "/gFZriCkpJYsApPZEF3jhxL4yLzG.jpg",
    genre_ids: [80, 18],
    id: 71446,
    media_type: "tv",
    name: "Money Heist",
    origin_country: ["ES"],
    original_language: "es",
    original_name: "La Casa de Papel",
    overview:
      "To carry out the biggest heist in history, a mysterious man called The Professor recruits a band of eight robbers who have a single characteristic: none of them has anything to lose. Five months of seclusion - memorizing every step, every detail, every probability - culminate in eleven days locked up in the National Coinage and Stamp Factory of Spain, surrounded by police forces and with dozens of hostages in their power, to find out whether their suicide wager will lead to everything or nothing.",
    popularity: 378.857,
    poster_path: "/reEMJA1uzscCbkpeRJeTT2bjqUp.jpg",
    first_air_date: "2017-05-02",
    vote_average: 8.291,
    vote_count: 16446,
  },
];

export default Movies;
